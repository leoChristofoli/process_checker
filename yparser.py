import os
import logging
import yaml


class Watched(object):
    '''
    takes a yaml file as parameter

    ** Must call is_valid method to access name, path and bin attributes
    '''
    def __init__(self, filepath):
        self.filepath = filepath
        self.error = None
        self.to_yaml = self.to_yaml()
        self.have_recovery = False

    def is_valid(self):
        process = self.to_yaml
        if not 'name' in process.keys() or not 'path' in process.keys() or not 'bin' in process.keys():
            self.error = 'not a valid parser'
            return False
        self.name = process['name']
        self.path = process['path']
        self.bin = process['bin']
        if 'recovery' in process.keys():
            self.have_recovery = True
            self.recovery = process['recovery']
        return True

    def to_yaml(self):
        try:
            a = yaml.load(open(self.filepath, 'r').read())
        except Exception as err:
            self.error = 'not a valid yaml'
            return {'error': str(err)}
        return a

    def __repr__(self):
        return self.filepath


def directoty2dict(dir):
    '''
    params: dir
    List the directory with yaml files and return a list
    '''
    res = {}
    if not os.path.isdir(dir):
        raise Exception('Directory not found')
    _files = os.listdir(dir)
    for _file in _files:
        try:
            a = yaml.load(open(os.path.join(dir,_file), 'r').read())
            a['success'] = True
        except Exception as err:
            a = {'success': False, 'error': err}
        res[_file] = a
    return res


def directory_list(dir):
    res = []
    if not os.path.isdir(dir):
        raise Exception('Directory not found')

    _files = os.listdir(dir)
    for _file in _files:
        a = Watched(os.path.join(dir, _file))
        res.append(a)
    return res

if __name__ == '__main__':
    # print(directoty2dict(dir='processes'))
    directory_list('process')