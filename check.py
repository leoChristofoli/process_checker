import psutil
import os
import yparser
import multiprocessing as m
import time
import subprocess
import logging
import logging.config
from logger import LOGGING

logging.config.dictConfig(LOGGING)
logging.info('started')

def file_reader(filename):
    '''
    discontinued
    '''
    f = open(filename).readlines()
    f = [x.strip() for x in f if x[0] is not  '#']
    f = [x.split('|') for x in f]
    f = [x for x in f if len(x) == 2]
    return f



def process_match(pwd, exe):
    ps = {
        'is_running': False,
        'process': []
    }
    for p in psutil.process_iter():
        try:
            p.cwd()
        except:
            continue
        # print(str(pwd).strip() == str(p.cwd()).strip())
        if str(pwd).lower().strip() == str(p.cwd()).lower().strip():
            print(p.cmdline())
            ps['is_running'] = False
            if str(exe).lower().strip() in ' '.join(p.cmdline()).lower():
                ps['is_running'] = True
                ps['process'].append(p)
    return ps


def dirty_search(pwd, exe):
    ps = {
        'is_running': False,
        'process': []
    }
    for p in psutil.process_iter():
        try:
            p.cwd()
        except:
            continue
        # print(str(pwd).strip() == str(p.cwd()).strip())

        if str(exe).strip().lower() in ' '.join(p.cmdline()).lower():
            ps['is_running'] = 'dirty possible'
            ps['process'].append(p)
    return ps


def check(plist):
    logging.info('checking')
    to_check = yparser.directory_list(plist)
    for process in to_check:
        if process.is_valid():
            print(process.name)
            pr = process_match(pwd=process.path, exe=process.bin)
            if pr['is_running']:
                logging.info(process.filepath + ' is running')
                for p in pr['process']:
                    print(p.memory_full_info())
            else:
                print(process.have_recovery)
                logging.info(process.filepath + ' is not running')
                if process.have_recovery:
                    logging.info(process.filepath + ' have recovery plan')
                    fail_plan(_file=process.recovery)


def run(func):
    pass


def fail_plan(_file):
    logging.info('trying to recovery with ' + _file)
    recovery_file = os.path.join('recovery', _file)
    if os.path.isfile(recovery_file):
        logging.info(recovery_file + ' found')
        p = m.Process(target=subprocess.check_output, args=((recovery_file, )), name='test')
        p.start()
        logging.info(str(p)  + ' started')
    else:
        logging.info(_file + ' not found')


if __name__ == '__main__':
    while True:
        print('listening')
        check('process')
        logging.info('started')
        time.sleep(30)
